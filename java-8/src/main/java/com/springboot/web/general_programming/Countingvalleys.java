package com.springboot.web.general_programming;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

public class Countingvalleys {

	// Complete the countingValleys function below.
    static int countingValleys(int n, String s) {

    	AtomicInteger valleys=new AtomicInteger();
    	AtomicInteger count=new AtomicInteger();
    	s.chars().mapToObj(c->Character.toLowerCase(Character.valueOf((char) c)))
    	.forEach(c->{
    		if(c.equals('u')) {
    			count.incrementAndGet();
    		}
    		if(c.equals('d')) {
    			count.decrementAndGet();
    		}
    		if(count.get()==0 && c.equals('u')) {
    			valleys.incrementAndGet();
    		}
    	});
    	return valleys.get();
    	
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String s = scanner.nextLine();

        int result = countingValleys(n, s);

        System.out.println(result);
        scanner.close();
    }
}
